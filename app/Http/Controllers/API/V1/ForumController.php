<?php

namespace App\Http\Controllers\API\V1;

use Exception;
use App\Models\Forum;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $Forum = Forum::all();
            // $pagination = 2;
            // $Forum = $Forum->orderBy('created_at, desc')->paginate($pagination);

            $response = $Forum;
            $code = 200;
        }catch (Exception $e){
            $code = 500;
            $response = $e -> getMessage();
        }

        return apiResponseBuilder($code,$response);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'level' => 'required',
            'full_name' => 'required',
            'place_and_date_of_birth' => 'required',
            'previous_education' => 'required',
            'origin_school' => 'required',
            'child' => 'required',
            'number_of_siblings' => 'required',
            'achievements_owned' => 'required',
            'english_skills' => 'required',
            'memorize_alquran' => 'required',
            'computer_skills' => 'required',
            'your_strengths' => 'required',
            'fathers_name' => 'required',
            'mothers_name' => 'required',
            'living_area' => 'required',
            'home_address' => 'required',
            'phone_number' => 'required',
            'email' => 'required',
            'why_choose_this_school' => 'required',
            'where_you_get_this_information' => 'required',
            'image' => 'required | image | mimes:jpg,jpeg,png,gif'            
        ]);

        try{
            $image = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('image'), $image);

            $Forum = new Forum();

            $Forum->level = $request->level;
            $Forum->full_name = $request->full_name;
            $Forum->place_and_date_of_birth = $request->place_and_date_of_birth;
            $Forum->previous_education = $request->previous_education;
            $Forum->origin_school = $request->origin_school;
            $Forum->child = $request->child;
            $Forum->number_of_siblings = $request->number_of_siblings;
            $Forum->achievements_owned = $request->achievements_owned;
            $Forum->english_skills = $request->english_skills;
            $Forum->memorize_alquran = $request->memorize_alquran;
            $Forum->computer_skills = $request->computer_skills;
            $Forum->your_strengths = $request->your_strengths;
            $Forum->fathers_name = $request->fathers_name;
            $Forum->mothers_name = $request->mothers_name;
            $Forum->living_area = $request->living_area;
            $Forum->home_address = $request->home_address;
            $Forum->phone_number = $request->phone_number;
            $Forum->email = $request->email;
            $Forum->why_choose_this_school = $request->why_choose_this_school;
            $Forum->where_you_get_this_information = $request->where_you_get_this_information;
            $Forum->image = $request->image;

            $Forum->save();
            $code=200;
            $response=$Forum;

        } catch (Exception $e){
            if ($e instanceof ValidationException) {
                $code = 400;
                $response = 'tidak ada data';
            } else{
                $code = 500;
                $response=$e->getMessage();
            }
        }

        return apiResponseBuilder($code, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $Forum = Forum::findOrFail($id);

            $code = 200;
            $response = $Forum;
        }catch (\Exception $e){
            if ($e instanceof ModelNotFoundException) {
                $code = 404;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e -> getMessage();
            }
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'level' => 'required',
            'full_name' => 'required',
            'place_and_date_of_birth' => 'required',
            'previous_education' => 'required',
            'origin_school' => 'required',
            'child' => 'required',
            'number_of_siblings' => 'required',
            'achievements_owned' => 'required',
            'english_skills' => 'required',
            'memorize_alquran' => 'required',
            'computer_skills' => 'required',
            'your_strengths' => 'required',
            'fathers_name' => 'required',
            'mothers_name' => 'required',
            'living_area' => 'required',
            'home_address' => 'required',
            'phone_number' => 'required',
            'email' => 'required',
            'why_choose_this_school' => 'required',
            'where_you_get_this_information' => 'required',
            'image' => 'required'            
        ]);

        try{
            $Forum = new Forum();

            $Forum->level = $request->level;
            $Forum->full_name = $request->full_name;
            $Forum->place_and_date_of_birth = $request->place_and_date_of_birth;
            $Forum->previous_education = $request->previous_education;
            $Forum->origin_school = $request->origin_school;
            $Forum->child = $request->child;
            $Forum->number_of_siblings = $request->number_of_siblings;
            $Forum->achievements_owned = $request->achievements_owned;
            $Forum->english_skills = $request->english_skills;
            $Forum->memorize_alquran = $request->memorize_alquran;
            $Forum->computer_skills = $request->computer_skills;
            $Forum->your_strengths = $request->your_strengths;
            $Forum->fathers_name = $request->fathers_name;
            $Forum->mothers_name = $request->mothers_name;
            $Forum->living_area = $request->living_area;
            $Forum->home_address = $request->home_address;
            $Forum->phone_number = $request->phone_number;
            $Forum->email = $request->email;
            $Forum->why_choose_this_school = $request->why_choose_this_school;
            $Forum->where_you_get_this_information = $request->where_you_get_this_information;
            $Forum->image = $request->image;

            $Forum->save();
            $code=200;
            $response=$Forum;

        } catch (Exception $e){
            if ($e instanceof ValidationException) {
                $code = 400;
                $response = 'tidak ada data';
            } else{
                $code = 500;
                $response=$e->getMessage();
            }
        }

        return apiResponseBuilder($code, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $Forum = Forum::find($id);
            $Forum->delete();

            $code = 200;
            $response = $Forum;
        }catch(\Exception $e){
            $code = 500;
            $response = $e->getMessage(); 
        }
        return apiResponseBuilder($code,$response);
    }
}
