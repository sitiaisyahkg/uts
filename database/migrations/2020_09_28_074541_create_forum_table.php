<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('level');
            $table->string('full_name');
            $table->string('place_and_date_of_birth');
            $table->string('previous_education');
            $table->string('origin_school');
            $table->string('child');
            $table->string('number_of_siblings');
            $table->string('achievements_owned');
            $table->string('english_skills');
            $table->string('memorize_alquran');
            $table->string('computer_skills');
            $table->string('your_strengths');
            $table->string('fathers_name');
            $table->string('mothers_name');
            $table->string('living_area');
            $table->string('home_address');
            $table->string('phone_number')->unique();
            $table->string('email')->unique();
            $table->string('why_choose_this_school');
            $table->string('where_you_get_this_information');
            $table->string('image')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum');
    }
}
